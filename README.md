![visitors](https://visitor-badge.glitch.me/badge?page_id=page.id) 
[![](https://tokei.rs/b1/github/XAMPPRocky/tokei)](https://github.com/InnopolisUniversity/innometrics-dashboard/edit/master).
![GitHub issues](https://img.shields.io/github/issues/shaxri/NlpWithNeuralNetwork)
![Website](https://img.shields.io/website?up_color=red&up_message=Online&url=https%3A%2F%2Finnometrics.ru%2F%23innometrics-subscribe)
![GitHub forks](https://img.shields.io/github/forks/shaxri/NlpWithNeuralNetwork?style=social)
[![Yarn package](https://github.com/InnopolisUniversity/innometrics-dashboard/actions/workflows/CI.yaml/badge.svg)](https://github.com/InnopolisUniversity/innometrics-dashboard/actions/workflows/CI.yaml)


<p align="center">
<img width="233" height="58" src="https://github.com/InnopolisUniversity/innometrics-dashboard/blob/master/innometrics_logo.png">
</p>
<p align="center">
<img src="https://img.shields.io/badge/javascript%20-%23323330.svg?&style=for-the-badge&logo=javascript&logoColor=%23F7DF1E"/> <img src="https://img.shields.io/badge/react%20-%2320232a.svg?&style=for-the-badge&logo=react&logoColor=%2361DAFB"/> <img src="https://img.shields.io/badge/firebase%20-%23039BE5.svg?&style=for-the-badge&logo=firebase"/>
</p>


# Innometrics Frontend
Track, evaluate and analyze the software development process. This system provides a visualisation of users' working activity statistics. An intelligent, customizable dashboard to visualize and manipulate software engineering data based on requirements and preferences coming from the industry.
Stores information from:

- Desktop clients and browser extensions about users' activities (which programs, tabs and for how long user has used it)
- Remote repositories about developers' source code quality metrics


## Dashboard
Gaining insights from the development process has never been easier. [Dashboard](https://link.springer.com/chapter/10.1007/978-3-030-47240-5_16)’s primary purpose is to share important information in terms of different metrics to the end user.

## Features

- Charts
<p align="center">
<img width="700" height="400" src="https://github.com/InnopolisUniversity/innometrics-dashboard/blob/master/Innometrics_charts.png">
</p>

- Top applications per person daily
- Accumulated activities
- Accumulated total time spent
- Category of activities
  - Development
  - Education
  - Communication
  - Utilities
  - Management
  - Entertainment

## Links

* [Dashboard](https://innometrics-12856.firebaseapp.com/#/login)

* [Backend source code](https://github.com/InnopolisUniversity/innometrics-backend)

* [API](https://github.com/InnopolisUniversity/innometrics-backend/blob/master/documentation.yaml)
  (Access readable format by pasting `documentation.yaml` data to https://editor.swagger.io)
* [Here](https://drive.google.com/file/d/1ghOf4uXLN9Nl4MYenroQuLhQ3GPfZMZW/view?usp=sharing) you can read about PRIVACY NOTICE of Innometrics system.
*  [Website](https://innometrics.ru/)
<p align="center">
<img width="800" height="500" src="https://github.com/InnopolisUniversity/innometrics-dashboard/blob/master/Innometrics_website.png" alt="Innometrics website"></a>
<p>

## Installation

1. `yarn`
2. `yarn start` to boot up a development server
3. Visit the link found in your terminal after running `yarn start`.

## Have Question or Feedback?
Contact us via email info@innometric.guru

