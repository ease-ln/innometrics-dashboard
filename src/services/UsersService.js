import { authGET } from "./ApiService";

import { config } from "./config";

const getUsersRoute = `${config.URL}:${config.PORT_NUMBER}/${config.API.VERSION}/Admin/${config.API.USERS}`;

export const fetchUsers = (token) => {
  return authGET(getUsersRoute, config.CONTENT_TYPES.APPLICATION_JSON, token);
};
