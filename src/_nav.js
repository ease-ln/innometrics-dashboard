export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Users',
      url: '/users',
      icon: 'icon-people',
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'icon-settings',
    },
    {
      name: 'Data Control',
      url: '/control',
      icon: 'icon-shield',
    },
    {
      name: "Configuration",
      url: '/company',
      icon: 'icon-wrench',
      badge: {
        variant: 'success',
        text: 'NEW',
      },
    },
    {
      name: "Agent's Control",
      url: '/agentMenu',
      icon: 'icon-speech',
      badge: {
        variant: 'success',
        text: 'NEW',
      },
    },
    {
      name: 'GQM Config',
      url: 'gqm',
      icon: 'icon-question',
      badge: {
        variant: 'success',
        text: 'NEW',
      },
    },
    {
      name: 'Mac Collector',
      url:
        'https://innometric.guru/files/collectors-files/mac_collector/InnoMetricsCollectorV317.zip',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'info',
      attributes: {target: '_blank', rel: 'noopener'},
    },
    {
      name: 'Windows Collector',
      url:
        'https://innometric.guru/files/collectors-files/win_collector/setup.exe',
      icon: 'icon-cloud-download',
      variant: 'success',
      attributes: {target: '_blank', rel: 'noopener'},
    },
    {
      name: 'Linux Collector',
      url:
        'https://innometric.guru/files/collectors-files/linux_collector/dataCollector_1.0.2.zip',
      icon: 'icon-cloud-download',
      variant: 'muted',
      attributes: {target: '_blank', rel: 'noopener'},
    },
  ],
}
