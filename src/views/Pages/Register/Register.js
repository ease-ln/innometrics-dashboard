import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert,
} from "reactstrap";

import { withRouter } from "react-router-dom";

// redux
import { connect } from "react-redux";
import { registerFlow } from "../../../redux/common/flows";

import { isEmail } from "../Login/Login.helper";

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      surname: "",
      email: "",
      password: "",
      error: null,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  submitForm(e) {
    e.preventDefault();
    const { firstName, surname, email, password } = this.state;

    if (!isEmail(email)) {
      return this.toggle("Email not valid. Please check it again.");
    }

    if (password.length < 3) {
      return this.toggle("Password too short. Minimum 3 characters.");
    }

    return this.props
      .registerFlow(firstName, surname, email, password)
      .then((res) => {
        if (this.props.auth.registerError) {
          this.setState({
            error: this.props.auth.registerError,
          });
        }
        if (this.props.auth.registerSuccess) {
          this.props.history.push("/login");
        }
      });
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.submitForm.bind(this)}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    {this.state.error && (
                      <Alert color="danger">{this.state.error}</Alert>
                    )}
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        name="firstName"
                        type="text"
                        placeholder="First name"
                        autoComplete="given-name"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        name="surname"
                        type="text"
                        placeholder="Last name"
                        autoComplete="family-name"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input
                        name="email"
                        type="text"
                        placeholder="Email"
                        autoComplete="email"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        name="password"
                        type="password"
                        placeholder="Password"
                        autoComplete="new-password"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <Button color="success" block>
                      Create Account
                    </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default withRouter(
  connect(
    (state) => ({
      auth: state.auth.toJS(),
    }),
    (dispatch) => ({
      registerFlow: (firstName, surname, email, password) =>
        dispatch(registerFlow(firstName, surname, email, password)),
    })
  )(Register)
);
