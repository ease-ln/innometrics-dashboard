import React from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  FormGroup,
  Input,
  Label,
} from 'reactstrap'
import QuestionDnD from './QuestionDnD'

export default function GQMConfig(props) {
  const isReadyForProduction = false
  return (
    <Card>
      <CardHeader>
        <strong>GQM Configuration</strong>
      </CardHeader>
      <CardBody>
        {isReadyForProduction ? (
          <>
            <Row>
              <Col xs="12">
                <FormGroup>
                  <Label htmlFor="goal">Goal</Label>
                  <Input
                    type="text"
                    id="goal"
                    placeholder="Enter your goal"
                    required
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs="12">
                <QuestionDnD />
              </Col>
            </Row>
          </>
        ) : (
          <p>
            Currently in Development. For questions ask Dragos on telegram
            (@strudra)
          </p>
        )}
      </CardBody>
    </Card>
  )
}
