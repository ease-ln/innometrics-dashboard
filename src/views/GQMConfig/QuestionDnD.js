import React, {Component} from 'react'
import {Label} from 'reactstrap'
import {DragDropContext, Droppable, Draggable} from 'react-beautiful-dnd'

import {
  getListStyle,
  getItemStyle,
  grid,
  move,
  reorder,
  getItems,
} from './utils'

export default class QuestionDnD extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: getItems(6),
      selected: getItems(5, 10),
      bottom: [],
    }
    this.onDragEnd = this.onDragEnd.bind(this)
  }

  /**
   * A semi-generic way to handle multiple lists. Matches
   * the IDs of the droppable container to the names of the
   * source arrays stored in the state.
   */
  id2List = {
    droppable: 'items',
    droppable2: 'selected',
    droppable3: 'bottom',
  }

  getList = (id) => this.state[this.id2List[id]]

  onDragEnd = (result) => {
    const {source, destination} = result

    // dropped outside the list
    if (!destination) {
      return
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        this.getList(source.droppableId),
        source.index,
        destination.index,
      )

      let state = {items}

      if (source.droppableId === 'droppable2') {
        state = {selected: items}
      }

      if (source.droppableId === 'droppable3') {
        state = {bottom: items}
      }

      this.setState(state)
    } else {
      const result = move(
        this.getList(source.droppableId),
        this.getList(destination.droppableId),
        source,
        destination,
      )
      if (result.droppable) {
        this.setState({items: result.droppable})
      }
      if (result.droppable2) {
        this.setState({selected: result.droppable2})
      }
      if (result.droppable3) {
        this.setState({bottom: result.droppable3})
      }
    }
  }

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable" direction="horizontal">
          {(provided, snapshot) => (
            <>
              <Label>Question 1</Label>
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {this.state.items.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style,
                        )}
                      >
                        {item.content}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            </>
          )}
        </Droppable>

        <Droppable droppableId="droppable2" direction="horizontal">
          {(provided, snapshot) => (
            <>
              <Label>Question 2</Label>
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}
              >
                {this.state.selected.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style,
                        )}
                      >
                        {item.content}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            </>
          )}
        </Droppable>
        <Droppable droppableId="droppable3" direction="horizontal">
          {(provided, snapshot) => (
            <>
              <Label>Question 3</Label>
              <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver, true)}
              >
                {this.state.bottom.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style,
                        )}
                      >
                        {item.content}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            </>
          )}
        </Droppable>
      </DragDropContext>
    )
  }
}
