import React, {Component} from 'react'
import {Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'

import {connect} from 'react-redux'
import {fromJS} from 'immutable'

import './Dashboard.scss'
import 'react-datepicker/dist/react-datepicker.css'

import {getPieData, getPieOptions} from './PieHelper.js'
import {getLineData, getLineOptions} from './TimeHelper.js'
import {getCumulLineData, getCumulLineOpts} from './CumulHelper.js'
import {getBarData, getBarOptions} from './BarHelper'
import {
  timeChartData,
  thirdChartData,
  activitiesChartData,
  categoryChartData,
  fetchSQMetrics,
  fetchSQCoverage,
  fetchSQProjects,
} from '../../redux/common/flows.js'
import {
  groupByApp,
  groupByDate,
  gropByCategory,
  cumulteHourly,
} from './DashboardHelper.js'

import GQMWrapper from './GQM/GQMWrapper'

class Dashboard extends Component {
  today = new Date()
  state = {
    dropdownOpen: false,
    projectDropdownOpen: false,
    projectSelected: {
      projectID: null,
    },
  }

  componentDidMount() {
    this.projectMapPropsToState()
  }

  projectMapPropsToState = () => {
    if (
      this.props.projects.length > 0 &&
      !this.state.projectSelected.projectID
    ) {
      this.setProject(this.props.projects[0])
    }
  }

  // for time chart
  timeReturn = async (date, endDate) => {
    const timeByDate = await this.getTimeReportData(date, endDate)
    if (timeByDate) return Dashboard.returnLineData(timeByDate)
    return null
  }

  async getTimeReportData(date, endDate) {
    const r = await this.props.fetchTimeFlow(
      date,
      endDate,
      this.state.projectSelected.projectID,
    )
    return groupByDate(r.report)
  }

  coverageReturn = async () => {
    const data = await fetchSQProjects(this.state.projectSelected.projectID)
    console.log('got sq projects hahah)', data)
    return data //todo: this
  }

  // for cumulative (app hourly chart)
  cumulReturn = async (date, endDate, user) => {
    const fromCumulData = await this.getCumulReportData(date, endDate, user)
    if (fromCumulData && fromCumulData.hours && fromCumulData.apps) {
      const {hours, apps} = fromCumulData
      const cumulData = getCumulLineData(hours, apps)
      const cumulOpts = getCumulLineOpts()

      if (cumulData && cumulOpts)
        return {
          data: cumulData,
          options: cumulOpts,
        }
    }
    return {data: {labels: []}}
  }

  async getCumulReportData(date, endDate, user) {
    const r = await this.props.fetchThirdFlow(
      user,
      date,
      date,
      this.state.projectSelected.projectID,
    )
    return cumulteHourly(r.activityReports)
  }

  // for activity chart
  activityReturn = async (date, endDate) => {
    const timeByApp = await this.getActivityReportData(date, endDate)
    return Dashboard.returnPieData(timeByApp)
  }

  async getActivityReportData(date, endDate) {
    const act = await this.props.fetchActivitiesFlow(
      date,
      endDate,
      this.state.projectSelected.projectID,
    )
    return groupByApp(act.report)
  }

  // for category report
  categoryReturn = async (date, endDate) => {
    const fromCatData = await this.getCategoryReportData(date, endDate)
    const catData = getBarData(fromCatData)
    const catOpts = getBarOptions()
    return {data: catData, options: catOpts}
  }

  async getCategoryReportData(date, endDate) {
    const r = await this.props.fetchCategoryFlow(
      date,
      endDate,
      this.state.projectSelected.projectID,
    )
    return gropByCategory(r.report)
  }

  static returnPieData = (timeByApp) => {
    const pieData = getPieData(timeByApp)
    const pieOptions = getPieOptions()
    return {
      data: pieData,
      options: pieOptions,
    }
  }

  static returnLineData = (timeByDate) => {
    const timeData = getLineData(timeByDate)
    const timeOpts = getLineOptions()
    return {
      data: timeData,
      options: timeOpts,
    }
  }

  static returnBarData = () => {}

  projectDropdownToggle = () =>
    this.setState({
      projectDropdownOpen: !this.state.projectDropdownOpen,
    })
  setProject = (p) => this.setState({projectSelected: p})

  toggle = () => {
    this.setState({dropdownOpen: !this.state.dropdownOpen})
  }

  Loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  )

  render() {
    return (
      <div className="animated fadeIn table-responsive">
        <Dropdown
          isOpen={this.state.projectDropdownOpen}
          toggle={this.projectDropdownToggle}
          style={{marginBottom: '0.7rem'}}
        >
          <DropdownToggle caret color="light">
            {this.state.projectSelected
              ? this.state.projectSelected.name
              : 'Project select'}
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem header>Projects</DropdownItem>
            {this.props.projects.map((p) => (
              <DropdownItem key={p.name} onClick={() => this.setProject(p)}>
                {p.name}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </Dropdown>
        <GQMWrapper
          goal="Maximize Productivity While Minimizing Waste"
          questions={[
            {
              name: 'What is the Overall Time Spent?',
              metrics: [
                {
                  id: 0,
                  name: 'Accumulated Total Time Spent',
                  type: 'line',
                  userSelectEnabled: false,
                  withEndDate: true,
                  api: this.timeReturn,
                },

                {
                  id: 1,
                  name: 'Accumulated Activities',
                  type: 'pie',
                  userSelectEnabled: false,
                  withEndDate: true,
                  api: this.activityReturn,
                },
              ],
            },

            {
              name: 'What Type of Apps do Users Mostly Use?',
              metrics: [
                {
                  id: 2,
                  name: 'Top 5 Apps Per Person Daily',
                  type: 'line',
                  userSelectEnabled: true,
                  withEndDate: false,
                  api: this.cumulReturn,
                },
                {
                  id: 3,
                  name: 'Category Chart',
                  type: 'bar',
                  userSelectEnabled: false,
                  withEndDate: true,
                  api: this.categoryReturn,
                },
              ],
            },

            {
              name: 'What is the Overall Code Quality?',
              metrics: [
                {
                  id: 4,
                  name: 'SonarQube Coverage',
                  type: 'number',
                  userSelectEnabled: true,
                  withEndDate: false,
                  api: this.coverageReturn,
                },
              ],
            },
          ]}
          userRole={this.props.role}
          users={this.props.users}
          projectID={this.state.projectSelected.projectID}
        />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    users: fromJS(state.users.get('users')).toJS(),
    activities: fromJS(state.report.get('activities')).toJS(),
    timeReport: fromJS(state.report.get('time')).toJS(),
    cumulReport: fromJS(state.report.get('cumul')).toJS(),
    activitiesLastFetched: fromJS(state.report.get('lastFetched')),
    projects: fromJS(state.projects.get('projects')).toJS(),
    role: fromJS(state.roles.get('role')),
  }),
  (dispatch) => ({
    fetchThirdFlow: (email, date = new Date(), endDate, projectId) =>
      dispatch(thirdChartData(email, date, endDate, projectId)),
    fetchTimeFlow: (date = new Date(), endDate, projectId) =>
      dispatch(timeChartData(date, endDate, projectId)),
    fetchActivitiesFlow: (date = new Date(), endDate, projectId) =>
      dispatch(activitiesChartData(date, endDate, projectId)),
    fetchCategoryFlow: (date = new Date(), endDate, projectId) =>
      dispatch(categoryChartData(date, endDate, projectId)),
  }),
)(Dashboard)
