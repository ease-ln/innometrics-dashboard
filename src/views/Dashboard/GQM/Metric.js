import React, {useState, useEffect} from 'react'

import Downshift from 'downshift'
import matchSorter from 'match-sorter'

import {Card, CardBody, CardHeader, Label, Alert, Spinner} from 'reactstrap'
import DatePicker from 'react-datepicker'
import CustomInput from '../CustomInput'
import UserSelect from '../UserSelect'
import Widget02 from '../../Widgets/Widget02'

import {Pie, Line, HorizontalBar} from 'react-chartjs-2'

export default function Metric({
  name,
  type = 'line',
  userSelect = null,
  startAndEndDate = false,
  userRole = 'DEVELOPER',
  users = [],
  api = null,
  projectID,
}) {
  const today = new Date()
  const [date, setDate] = useState(
    startAndEndDate
      ? new Date(today.setDate(today.getDate() - 30))
      : new Date(),
  )
  const [endDate, setEndDate] = useState(new Date())
  const [email, setEmail] = useState(
    JSON.parse(localStorage.getItem('innometrics-email')),
  )
  const [isOpen, setIsOpen] = useState(false)
  const [itemsToShow, setItemsToShow] = useState([])
  const [isChartLoading, setIsChartLoading] = useState(false)
  const [data, setData] = useState(null)
  const [options, setOptions] = useState(null)
  const [error, setError] = useState(null)

  function getItemsToShow(value) {
    return value
      ? matchSorter(users, value, {
          keys: ['email'],
        })
      : users
  }

  function handleStateChange(changes, downshiftState) {
    if (changes.hasOwnProperty('isOpen')) {
      var newOpen =
        changes.type === Downshift.stateChangeTypes.mouseUp
          ? newOpen
          : changes.isOpen

      if (newOpen) {
        setIsOpen(newOpen)
        setItemsToShow(getItemsToShow(downshiftState.inputValue))
      }
    } else if (changes.hasOwnProperty('inputValue')) {
      setItemsToShow(getItemsToShow(downshiftState.inputValue))
    }
  }

  function handleUserChange(selectedItem, downshiftState) {
    if (selectedItem && selectedItem.email) {
      setEmail(selectedItem.email)
      setIsOpen(false)
    }
  }

  useEffect(() => {
    let isMounted = true
    async function exec() {
      if (isMounted) setIsChartLoading(true)
      try {
        const apiData = await api(date, startAndEndDate ? endDate : date, email)
        if (isMounted) {
          setData(apiData.data)
          setOptions(apiData.options)
        }
      } catch (e) {
        console.error(e)
        setError('Something went wrong.')
      }
      setIsChartLoading(false)
    }

    exec()
    return () => {
      isMounted = false
    }
  }, [email, date, endDate, projectID])

  return (
    <Card>
      <DatePicker
        selected={date}
        onChange={(d) => setDate(d)}
        dateFormat="dd/mm/yyyy"
        maxDate={new Date()}
        customInput={
          <CustomInput
            readOnly
            date={date}
            text={startAndEndDate ? 'Start date' : 'Date'}
          />
        }
      />

      {startAndEndDate && (
        <DatePicker
          selected={endDate}
          onChange={(d) => setEndDate(d)}
          dateFormat="dd/mm/yyyy"
          maxDate={new Date()}
          minDate={date}
          customInput={<CustomInput readOnly date={endDate} text="End date" />}
        />
      )}

      {userSelect && userRole !== 'DEVELOPER' && (
        <UserSelect
          onStateChange={handleStateChange}
          isOpen={isOpen}
          onChange={handleUserChange}
          items={itemsToShow}
          itemToString={itemToString}
        />
      )}

      <CardHeader>
        {name}
        <div className="card-header-actions"></div>
      </CardHeader>
      <CardBody>
        <div className="chart-wrapper" style={{height:"250px"}}>
          {error && !email ? (
            <Label>{error}</Label>
          ) : !data || isChartLoading ? (
            <Spinner animation="grow" variant="primary" />
          ) : data.labels.length === 0 ? (
            <Alert color="warning">No data.</Alert>
          ) : type === 'line' ? (
            <Line data={data} options={options} />
          ) : type === 'bar' ? (
            <HorizontalBar data={data} options={options} />
          ) : type === 'number' ? (
            <Widget02
              header="$1.999,50"
              mainText="Income"
              icon="fa fa-cogs"
              color="primary"
              className="card-body-unset"
              variant="1"
            />
          ) : (
            <Pie data={data} options={options} />
          )}
        </div>
      </CardBody>
    </Card>
  )
}

function itemToString(i) {
  return i ? i.email : ''
}
