import React, {Component} from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
  Input,
} from 'reactstrap'
import {withRouter} from 'react-router-dom'

import {connect} from 'react-redux'
import {fromJS} from 'immutable'

class User extends Component {
  state = {
    socialMedia: '',
    gender: '',
    birthDate: '',
  }

  handleChange = (k) => (e) => {
    const lower = k.toLowerCase()
    const key =
      lower === 'social media'
        ? 'socialMedia'
        : lower === 'date of birth'
        ? 'birthDate'
        : 'gender'
    this.setState({[key]: e.target.value})
  }

  render() {
    const userList = this.props.users.map((u, i) => ({...u, id: i}))
    const myEmail = localStorage.getItem('email')
    const isMyProfilePage =
      this.props.location.pathname.split('/').join('').trim() === 'me'

    const user = isMyProfilePage
      ? userList.find((user) => user.email === myEmail)
      : userList.find(
          (user) => user.id.toString() === this.props.match.params.id,
        )

    const userDetails = user
      ? Object.entries(user)
      : [
          [
            'id',
            <span>
              <i className="text-muted icon-ban"></i> Not found
            </span>,
          ],
        ]

    const {socialMedia, gender, birthDate} = this.state
    const userProfileStateData = [socialMedia, gender, birthDate]

    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong>
                  <i className="icon-info pr-1"></i>User id:{' '}
                  {isMyProfilePage ? user.id : this.props.match.params.id}
                </strong>
              </CardHeader>
              <CardBody>
                <Table responsive striped hover>
                  <tbody>
                    {userDetails.map(([key, value]) => {
                      return (
                        <tr key={key}>
                          <td>{`${key}:`}</td>
                          <td>
                            <strong>{value}</strong>
                          </td>
                        </tr>
                      )
                    })}
                    {isMyProfilePage &&
                      ['Social Media', 'Gender', 'Date of Birth'].map(
                        (k, idx) => (
                          <tr key={k}>
                            <td>{`${k}:`}</td>
                            <td>
                              <Input
                                type="text"
                                name={k}
                                value={userProfileStateData[idx]}
                                onChange={this.handleChange(k)}
                              />
                            </td>
                          </tr>
                        ),
                      )}
                  </tbody>
                </Table>

                {isMyProfilePage && (
                  <>
                    <Button color="secondary" disabled>
                      Save
                    </Button>
                    <Button color="link" className="ml-1" disabled>
                      Change password
                    </Button>
                  </>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default withRouter(
  connect((state) => ({
    users: fromJS(state.users.get('users')).toJS(),
  }))(User),
)
