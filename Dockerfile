FROM node:13-alpine
EXPOSE 8000
WORKDIR /app
ADD . .
RUN yarn install
CMD ["yarn", "start"]